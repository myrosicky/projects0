package org.ll.ui;

import java.awt.ComponentOrientation;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.ll.Client;

public class queryWindow {

	private JFrame frmsql;
	private Client client  = new Client() ;
	private final Action action = new SwingAction();;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
				try {
					queryWindow window = new queryWindow();
					window.frmsql.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
//			}
//		});
	}

	/**
	 * Create the application.
	 * @throws Exception 
	 */
	public queryWindow() throws Exception {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmsql = new JFrame();
		frmsql.setLocation(new Point(400, 4400));
		frmsql.setTitle("\u8FDC\u7A0B\u67E5sql");
		frmsql.setBounds(new Rectangle(200, 200, 500, 500));
		frmsql.setBounds(0, 0, 758, 340);
		frmsql.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmsql.getContentPane().setLayout(null);
		frmsql.setLocationRelativeTo(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(68, 10, 605, 180);
		frmsql.getContentPane().add(scrollPane);
		
		final JTextArea textSql = new JTextArea();
		scrollPane.setViewportView(textSql);
		textSql.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		
		JLabel lblSql = new JLabel("SQL");
		lblSql.setBounds(25, 23, 54, 15);
		frmsql.getContentPane().add(lblSql);
		

		JButton btnNewButton = new JButton("\u67E5\u8BE2(q)");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					
					client.query(textSql.getText());
					
					File file1= new File(client.getTempFileParent(),client.getTempFileName());
//					jepResult.setEditable(false);
//					jepResult.setContentType("text/html");
//					System.err.println(file1.toURI().toString());
//					System.err.println(file1.toURI().toURL().toString());
//					jepResult.setPage(file1.toURI().toURL());
					Process process = Runtime.getRuntime().exec("cmd /c start "+ file1.getAbsolutePath());
					process.waitFor();
					process.destroy();
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, e1.getMessage());
				}
			}
		});
		btnNewButton.setMnemonic(KeyEvent.VK_Q);
		btnNewButton.setToolTipText("\u67E5\u8BE2(q)");
		btnNewButton.setBounds(319, 200, 93, 23);
		frmsql.getContentPane().add(btnNewButton);
	}
	private class SwingAction extends AbstractAction {
		public SwingAction() {
			putValue(NAME, "SwingAction");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}
		public void actionPerformed(ActionEvent e) {
		}
	}
}
