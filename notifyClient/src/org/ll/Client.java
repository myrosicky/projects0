package org.ll;
import java.awt.HeadlessException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.rmi.ConnectException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;

import rmi.ServerProxy;


public class Client {

	private Socket socket;
	private String targetHost = "192.168.54.53";//"192.168.54.37";
	private int targetPort = 10086;
	private String name;
	private String interrupttedName ;
	private final String tempFileParent = "./temp";
	private final String tempFileName = "tempResult.html";
	ServerProxy server = null;
	
	public String getTempFileParent() {
		return tempFileParent;
	}

	public String getTempFileName() {
		return tempFileName;
	}

	public String getInterrupttedName() {
		return interrupttedName;
	}

	public void setInterrupttedName(String interrupttedName) {
		this.interrupttedName = interrupttedName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Socket getSocket() {
		return socket;
	}

	public void setSocket(Socket socket) {
		this.socket = socket;
	}

	public String getTargetHost() {
		return targetHost;
	}

	public void setTargetHost(String targetHost) {
		this.targetHost = targetHost;
	}

	public int getTargetPort() {
		return targetPort;
	}

	public void setTargetPort(int targetPort) {
		this.targetPort = targetPort;
	}
	private void initServer() throws Exception{
//		if (server!=null)  return;
		try {
			 if (System.getSecurityManager() == null) {
		            System.setSecurityManager(new SecurityManager());
		        }
			
           String name = "notifyServer";
           Registry registry = LocateRegistry.getRegistry(targetHost);
           server = (ServerProxy) registry.lookup(name);
       } catch (ConnectException e) {
       	e.printStackTrace();
       	throw new Exception("连接服务端出错~");
       } catch (Exception e) {
           e.printStackTrace();
       }
	}

	public void close() {
		server = null;
		System.gc();
	}
	
	public Client() throws Exception{
//		try {
//			
			File file = new File(tempFileParent);
			if (!file.isDirectory()) {
				boolean rtnflag = file.mkdir();
				System.err.println(rtnflag);
			}
			initServer();
//			 if (System.getSecurityManager() == null) {
//		            System.setSecurityManager(new SecurityManager());
//		        }
//			
//           String name = "notifyServer";
//           Registry registry = LocateRegistry.getRegistry(targetHost);
//           server = (ServerProxy) registry.lookup(name);
//       } catch (ConnectException e) {
//       	e.printStackTrace();
//       	throw new Exception("连接服务端出错~");
//       } catch (Exception e) {
//           e.printStackTrace();
//       }
		
	}
	
	public boolean isFreeForLogin() throws Exception  {
		try {
			initServer();
            return !server.isInterruptted();
        } catch (ConnectException e) {
        	e.printStackTrace();
        	throw new Exception("连接服务端出错~");
        } catch (java.net.ConnectException e) {
        	e.printStackTrace();
        	throw new Exception("连接服务端出错~");
        }catch (Exception e) {
            e.printStackTrace();
        }
		return false;
	}
	
	public void loginSSL() throws Exception  {
		try {
			initServer();
            System.out.println(server.loginSSL());
        } catch (ConnectException e) {
        	e.printStackTrace();
        	throw new Exception("连接服务端出错~");
        } catch (Exception e) {
            e.printStackTrace();
        }
	}
	
	public void download(String resourceName) throws Exception  {
		try {
			initServer();
			byte[] filecontent = server.downloadResource(resourceName.substring(1));
			resourceName = resourceName.substring(resourceName.lastIndexOf("/")+1);
			File file = new File(tempFileParent, resourceName);
			if (!file.exists()) {
				file.createNewFile();
			}
			OutputStreamWriter os = new OutputStreamWriter(new FileOutputStream(file),"GBK");
			os.write(new String(filecontent));
			os.flush();
			os.close();
        } catch (ConnectException e) {
        	e.printStackTrace();
        	throw new Exception("连接服务端出错~");
        } catch (Exception e) {
            e.printStackTrace();
        }
	}
	
	
	
	
	/*
	public boolean isFreeForLogin() throws Exception  {
		
		try {
			socket = new Socket(InetAddress.getByName(targetHost),targetPort);
			boolean rtn = false;
			sendSignalMessage(socket,SystemConstant.ASK_FOR_LOGIN);
			
			String oneLine = recieveMessage(socket);
			int recvSignal = Integer.parseInt(oneLine);
			System.err.println("recvSignal  : " + recvSignal);
			if (recvSignal==SystemConstant.FREE) {
				rtn = true;
			}else if (recvSignal==SystemConstant.INTERRUPTTED) {
				rtn = false;
				//setInterrupttedName(recieveMessage(socket));
				System.err.println(getInterrupttedName());
			}
			socket.close();
			return rtn;
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//JOptionPane.showMessageDialog(null, "连接服务端出错~");
			throw new Exception("连接服务端出错~");
		} 
		return false;
	}
	*/
	public void remoteLogin() {
		try {
			Process process = Runtime.getRuntime().exec(" cmd /c mstsc /v " + targetHost);
			process.wait(2500);
			process.destroy();
		} catch(IllegalMonitorStateException e){
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	
	private void sendSignalMessage(Socket socket,int signalMessage) throws IOException {
		PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
		System.err.println(" send Message : " + signalMessage);
		writer.println(signalMessage);
		writer.flush();
		writer = null;
	}
	
	private void sendMessage(Socket socket,String message) throws IOException {
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
		System.err.println(" send Message : " + message);
		writer.write(message);
		writer.flush();
		writer = null;
	}
	
	private String recieveMessage(Socket socket) {
		String oneLine = null;
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			while ((oneLine = reader.readLine())==null) {
			}
			reader = null;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return oneLine;
	}
	
	private void log(String msg) {
		System.err.println(msg);
	}
	
	public boolean query(String sql) throws Exception {
		try {
			String rtnhtml = server.querySQL(sql);
			Pattern p = Pattern.compile("/gdld(/[^<>]*)+\\.(js|css)\"");
			Matcher matcher = p.matcher(rtnhtml);
			while (matcher.find()) {
				String resourceName = matcher.group();
				String resourceSimpleName = resourceName.substring(resourceName.lastIndexOf("/")+1);
				if (!new File(tempFileParent,resourceSimpleName.substring(0, resourceSimpleName.length()-1)).exists()) {
					download(resourceName.substring(0,resourceName.length()-1));
				}
				rtnhtml = rtnhtml.replace(rtnhtml.substring(matcher.start(), matcher.end()),resourceSimpleName);
				matcher = p.matcher(rtnhtml);
			}
			File file = new File(tempFileParent,tempFileName);
			if (!file.exists()) {
				file.createNewFile();
			}
//			System.err.println(rtnhtml);
			OutputStreamWriter os = new OutputStreamWriter(new FileOutputStream(file),"GBK");
			os.write(rtnhtml);
			os.flush();
			os.close();
            return true;
        } catch (ConnectException e) {
        	e.printStackTrace();
        	throw new Exception("连接服务端出错~");
        } catch (Exception e) {
            e.printStackTrace();
        }
		return false;
	}
	
	
	
	public static void main(String[] args) throws Exception {
		Client client= new Client();
		try {
//			if (client.isFreeForLogin()) {
//				client.remoteLogin();
//			}else {
//				JOptionPane.showMessageDialog(null, (client.getInterrupttedName()==null?"别人":client.getInterrupttedName())+"正在占用远程，等等吧");
//			}
			client.query("select * from s_u_user t where rownum<=12");
			
//		client.loginSSL();
//		Thread.sleep(10000);
//		client.query("select * from hc39 t where rownum<=5");
		} catch (HeadlessException e1) {
			e1.printStackTrace();
		} catch (Exception e1) {
			e1.printStackTrace();
			JOptionPane.showMessageDialog(null, e1.getMessage());
		}

	}

}
