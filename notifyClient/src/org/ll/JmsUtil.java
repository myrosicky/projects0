package org.ll;

import java.util.Hashtable;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Session;
import javax.jms.Topic;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class JmsUtil {

	private Hashtable<String, String> env;
	private ConnectionFactory myFactoryl;
	private Topic topic;

	public JmsUtil() {
		super();
		env = new Hashtable<String, String>();
		env.put(Context.INITIAL_CONTEXT_FACTORY,
				"com.sun.jndi.fscontext.RefFSContextFactory");
		env.put(Context.PROVIDER_URL, "file://E:/myworkspace/notifyClient/adminObjects");
	}

	public Topic getTopic() throws NamingException {
		topic = topic == null ? topic = (Topic) new InitialContext(env)
				.lookup("jms/Topic") : topic;
		return topic;
	}

	public ConnectionFactory getConnectionFactory() throws NamingException {
		myFactoryl = myFactoryl == null ? myFactoryl = (ConnectionFactory) new InitialContext(
				env).lookup("MyConnectionFactory") : myFactoryl;
		return myFactoryl;
	}

	public static void main(String[] args) throws JMSException, NamingException {
		JmsUtil jmsUtil = new JmsUtil();
		Topic topic = jmsUtil.getTopic();
		System.err.println("topic.getTopicName() == "+topic.getTopicName());
		Connection conn = jmsUtil.getConnectionFactory().createConnection("id",
				"password");
		Session session = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
		conn.start();

	}
}
