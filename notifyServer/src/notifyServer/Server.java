package notifyServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.rmi.AccessException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.RemoteObject;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashSet;
import java.util.Set;

import rmi.ServerImpl;
import rmi.ServerProxy;

public class Server extends RemoteObject {

	//private static final Logger logger = LogManager.getLogger(Server.class);
	Socket serverSocket;
	String host = "127.0.0.1"; //"192.168.54.53";
	int port = 10086 ;
	private int timeOut = 1000 * 60 * 30;
	ServerSocket server = null;
	Set<SocketAddress> askForLoginUsers ;
	
	
	public Socket getServerSocket() {
		return serverSocket;
	}

	public void setServerSocket(Socket serverSocket) {
		this.serverSocket = serverSocket;
	}


	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	
	public Server(){
		askForLoginUsers = new HashSet<SocketAddress>();
	}
	
	public void startServer() {
		System.err.println("SERVER启动中....");
		 try {
			 if (System.getSecurityManager() == null) {
				    System.setSecurityManager(new SecurityManager());
				}
			 String name = "notifyServer" ;
			 ServerProxy serverProxy = new ServerImpl();
			 ServerProxy stub =  (ServerProxy) UnicastRemoteObject.exportObject(serverProxy, 0);
			 Registry registry = LocateRegistry.getRegistry();
			 registry.rebind(name,  stub);
			 
			 
		} catch (AccessException e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	
	/*
	public void startServer() {
		//logger.error("SERVER启动中....");
		System.err.println("SERVER启动中....");
		try {
			server = new ServerSocket( port);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		Thread t1 = new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					while (!server.isClosed()) {
						server.setSoTimeout(timeOut);
						try {
							serverSocket = server.accept();
						} catch (SocketTimeoutException e) {
							e.printStackTrace();
							//logger.error(e.getMessage());
							continue;
						}
						String currentContactUser = serverSocket.getRemoteSocketAddress().toString();
						currentContactUser = currentContactUser.substring(0,currentContactUser.indexOf(":"));
						System.err.println("正在与" + currentContactUser + "通信");
						BufferedReader reader = new BufferedReader(new InputStreamReader(serverSocket.getInputStream()));
						String oneLine = reader.readLine();
						while (oneLine==null) {
							oneLine = reader.readLine();
						}
						int requestMsg = Integer.parseInt(oneLine);
						System.err.println("recev Msg: " + requestMsg);
						
						PrintWriter writer = new PrintWriter(new OutputStreamWriter(serverSocket.getOutputStream()));
						if (requestMsg==SystemConstant.ASK_FOR_LOGIN) {
							if (isInterruptted()) {
								askForLoginUsers.add(serverSocket.getRemoteSocketAddress());
								System.err.println("send Msg: " + SystemConstant.INTERRUPTTED);
								writer.println(SystemConstant.INTERRUPTTED);
								//writer.println("interruptedName");
							}else {
								System.err.println("send Msg: " + SystemConstant.FREE);
								writer.println(SystemConstant.FREE);
							}
						}
						writer.flush();
						serverSocket.close();
						System.err.println("断开与" + currentContactUser + "的连接");
						
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
				
			}
		});
		t1.start();
//		try {
//			t1.join();
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
	*/
	/*
	public void endServer()  {
		System.err.println("SERVER关闭中....");
		try {
			server.close();
			System.err.println("SERVER关闭完成....");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	*/
	
	public void endServer()  {
			System.err.println("SERVER关闭完成....");
	}
	
	public boolean isInterruptted() throws IOException {
		 String userName =  System.getProperty("user.name");
		 try {
			 Process process = Runtime.getRuntime().exec("cmd /c quser "+userName);
			 BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream())) ;
			 String oneLine = null;
			 while ((oneLine = reader.readLine())!=null) {
				 System.err.println("cmd:"+oneLine);
				String[] columns = oneLine.split("\\s{1,}");
				if (columns[0].contains(userName.toLowerCase())) {
					String remoteHostName = columns[1].trim();
					System.err.println("remoteHostName:"+remoteHostName);
					return remoteHostName.startsWith("rdp-tcp#");
				}
			 }
			 reader.close();
		 
			 process.waitFor();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 System.err.println("current login user is : "+ userName);
		 //System.err.println("current tasklist : "+ Runtime.getRuntime().exec("cmd /c tasklist "));
		 
		return false;
	}

	public boolean isClose() {
		return server==null?true:server.isClosed();
	}

	public static void main(String[] args) {
		
		
		try {
			new Server().startServer();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
