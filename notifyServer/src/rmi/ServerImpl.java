package rmi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ServerImpl implements ServerProxy {
	
	private static final Logger logger = LoggerFactory.getLogger(ServerImpl.class);
	
//	private CloseableHttpClient client;
//	private HttpClientContext context;
	
//	public void setClient(CloseableHttpClient client) {
//		this.client = client;
//	}
	
	private CloseableHttpClient client = HttpClients.custom()
			.disableAutomaticRetries()
			.setUserAgent("Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET4.0C; .NET4.0E; .NET CLR 2.0.50727)")
			.disableContentCompression()
			.build();
	
	@Override
	public boolean isInterruptted() throws IOException {
		 String userName =  System.getProperty("user.name");
		 try {
			 Process process = Runtime.getRuntime().exec("cmd /c quser "+userName);
			 BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream())) ;
			 String oneLine = null;
			 while ((oneLine = reader.readLine())!=null) {
				 logger.info("cmd:"+oneLine);
				String[] columns = oneLine.split("\\s{1,}");
				if (columns[0].contains(userName.toLowerCase())) {
					String remoteHostName = columns[1].trim();
					logger.info("remoteHostName:"+remoteHostName);
					return remoteHostName.startsWith("rdp-tcp#");
				}
			 }
			 reader.close();
		 
			 process.waitFor();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		 logger.info("current login user is : "+ userName);
		 //logger.info("current tasklist : "+ Runtime.getRuntime().exec("cmd /c tasklist "));
		 
		return false;
	}
	
	@Override
	public int loginSSL()  throws Exception{
//		
//		
//		java.util.logging.Logger.getLogger("org.apache.http.headers").setLevel(
//				java.util.logging.Level.FINER);
//
//		System.setProperty("org.apache.commons.logging.Log",
//				"org.apache.commons.logging.impl.SimpleLog");
//		System.setProperty("org.apache.commons.logging.simplelog.showdatetime",
//				"true");
//		System.setProperty(
//				"org.apache.commons.logging.simplelog.log.org.apache.http.headers",
//				"debug");
//		
//		String url = "";
//		HttpPost post = null;
//		HttpGet get = null;
//		HttpResponse response = null;
//		KeyStore trustStore  = KeyStore.getInstance(KeyStore.getDefaultType());
//        FileInputStream instream = new FileInputStream(new File("./sslCer.keystore"));
//        try {
//            trustStore.load(instream, "fuckyou".toCharArray());
//        } finally {
//            instream.close();
//        }
//
//        // Trust own CA and all self-signed certs
//        SSLContext sslcontext = SSLContexts.custom()
//                .loadTrustMaterial(trustStore, new TrustSelfSignedStrategy())
//                .build();
//        // Allow TLSv1 protocol only
//        SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
//                sslcontext,
//                new String[] { "TLSv1" },
//                null,
//                SSLConnectionSocketFactory.BROWSER_COMPATIBLE_HOSTNAME_VERIFIER);
//        CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslsf).build()
//                ;
//        setClient(httpClient);
//        context = HttpClientContext.create();
//		try {
//			System.out.println("first visit");
//			get = new HttpGet("https://121.33.199.78/por/index.csp");
//			get.addHeader("User-Agent","Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET4.0C; .NET4.0E; .NET CLR 2.0.50727)");
////			get.addHeader("Cookie","collection=%7Bauto_login_count%3A0%7D; g_LoginPage=login_psw; VisitTimes=0; haveLogin=0; ENABLE_RANDCODE=0; HARDID=3419D52DB3A5AA67AFCA7838D26E48233419D52DB3A5AA67AFCA7838D26E4823B2251E6758CDFA60102E0FA8F3B88C75B2251E6758CDFA60102E0FA8F3B88C756E746BAAEA721B1BDF5937968945CC73; LoginMode=2; allowlogin=1; TWFID=0b504e2f5c6c9fad; language=zh_CN; webonly=0; VpnLine=https%3A%2F%2F121.33.199.78%2Fpor%2Flogin_psw.csp");
//			get.addHeader("Accept","application/x-shockwave-flash, image/gif, image/jpeg, image/pjpeg, image/pjpeg, application/xaml+xml, application/x-ms-xbap, application/x-ms-application, */*");
//			get.addHeader("Accept-Language","zh-cn");
//			get.addHeader("Accept-Encoding","gzip, deflate");
//			try {
//				response = httpClient.execute(get,context);
//				System.out.println("context.getUserToken()��"+context.getUserToken());
//				get.abort();
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//			
//			
//			String hardidLocation = "";
//			ArrayList<NameValuePair> pairs = new ArrayList<NameValuePair>();
//			
//			pairs.add(new BasicNameValuePair("mitm_result", ""));
//			pairs.add(new BasicNameValuePair("svpn_name", "huazi"));
//			pairs.add(new BasicNameValuePair("svpn_password", "huazi"));
//			pairs.add(new BasicNameValuePair("svpn_rand_code", ""));
//			
//			post = new HttpPost("https://121.33.199.78/por/login_psw.csp?sfrnd=2346912324982305&encrypt=0");
//			post.addHeader("User-Agent","Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET4.0C; .NET4.0E; .NET CLR 2.0.50727)");
//			post.addHeader("Accept","application/x-shockwave-flash, image/gif, image/jpeg, image/pjpeg, image/pjpeg, application/xaml+xml, application/x-ms-xbap, application/x-ms-application, */*");
//			post.addHeader("Cache-Control","no-cache");
//			post.addHeader("Connection","Keep-Alive");
//			post.addHeader("Content-Type","application/x-www-form-urlencoded");
////			post.addHeader("Cookie","collection=%7Bauto_login_count%3A0%7D; g_LoginPage=login_psw; VisitTimes=0; haveLogin=0; ENABLE_RANDCODE=0; HARDID=3419D52DB3A5AA67AFCA7838D26E48233419D52DB3A5AA67AFCA7838D26E4823B2251E6758CDFA60102E0FA8F3B88C75B2251E6758CDFA60102E0FA8F3B88C756E746BAAEA721B1BDF5937968945CC73; LoginMode=2; allowlogin=1; TWFID=0b504e2f5c6c9fad; language=zh_CN; webonly=0; VpnLine=https%3A%2F%2F121.33.199.78%2Fpor%2Flogin_psw.csp");
//			post.addHeader("Cookie","collection=%7Bauto_login_count%3A0%7D; g_LoginPage=login_psw; VisitTimes=0; haveLogin=0; ENABLE_RANDCODE=0; HARDID=3419D52DB3A5AA67AFCA7838D26E48233419D52DB3A5AA67AFCA7838D26E4823B2251E6758CDFA60102E0FA8F3B88C75B2251E6758CDFA60102E0FA8F3B88C756E746BAAEA721B1BDF5937968945CC73; LoginMode=2; allowlogin=1;  language=zh_CN; webonly=0; VpnLine=https%3A%2F%2F121.33.199.78%2Fpor%2Flogin_psw.csp");
//			post.setEntity(new UrlEncodedFormEntity(pairs,"UTF-8"));
//			try {
//				response = httpClient.execute(post,context);
//				hardidLocation = response.getLastHeader("Location").getValue();
//				post.abort();
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//			
//			
//			get = new HttpGet("https://121.33.199.78/por/"+hardidLocation);
//			get.addHeader("User-Agent","Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET4.0C; .NET4.0E; .NET CLR 2.0.50727)");
//			get.addHeader("Accept","application/x-shockwave-flash, image/gif, image/jpeg, image/pjpeg, image/pjpeg, application/xaml+xml, application/x-ms-xbap, application/x-ms-application, */*");
//			try {
//				response = httpClient.execute(get);
//				get.abort();
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//			
			return 1;
//		} catch (Exception e) {
//			e.printStackTrace();
//			return -1;
//		}finally {
////			 httpClient.close();
//		}
//	
	}
	
	public CloseableHttpClient getClient() {
		return HttpClients.createDefault();
//		return this.client==null?HttpClients.createDefault():this.client;
	}
	
	@Override
	public String querySQL(String sql) throws Exception {
		
//		PoolingHttpClientConnectionManager manager = new PoolingHttpClientConnectionManager();
//		ConnectionReuseStrategy


		
		
		HttpPost post = null;
		HttpGet get = null;
		HttpResponse response = null;
		String html = "";
		ArrayList<NameValuePair> pairs = new ArrayList<NameValuePair>();
		String uri = "http://192.168.1.28:7001/gdld/query.jsp";
		try {
			/*****************************
			 * access index page, check if need logining first
			 *****************************/
//			logger.info("normal visit home page");
//			pairs.add(new BasicNameValuePair("haha","1"));
//			URI uuri = new URIBuilder(uri).addParameters(pairs).build();
//			get =  new HttpGet(uuri);
//			get.addHeader("Content-Type", "application/x-www-form-urlencoded");
//			logger.info("execute get request begin");
//			response = client.execute(get);
//			logger.info("execute get request end");
			
			logger.info("execute get request end");
			post =  new HttpPost(uri);
			post.addHeader("Content-Type", "application/x-www-form-urlencoded");
			post.addHeader("Accept-Encoding", "gzip, deflate");
			post.addHeader("Accept", "application/x-shockwave-flash, image/gif, image/jpeg, image/pjpeg, image/pjpeg, application/xaml+xml, application/x-ms-xbap, application/x-ms-application, */*");
				
			pairs = new ArrayList<NameValuePair>();
			pairs.add(new BasicNameValuePair("haha","1"));
			pairs.add(new BasicNameValuePair("isQuery","true"));
			pairs.add(new BasicNameValuePair("maxCol",""));
			pairs.add(new BasicNameValuePair("sqlStr",sql));
			post.setEntity(new UrlEncodedFormEntity(pairs,"GB2312"));
			response = client.execute(post);
			html = EntityUtils.toString(response.getEntity());
			post.abort();
			logger.info("check if needed login ");
			if (!html.contains("����")) 
				return html;
			
				/********************
				 * login 
				 ********************/
				logger.info("login");
				String hexStr  = "";
				for (byte ch : "�ύ��ѯ����".getBytes()) {
					hexStr += "%"+Integer.toHexString(ch & 0x000000ff).toUpperCase();
				}
				post =  new HttpPost(uri);
				post.addHeader("Content-Type", "application/x-www-form-urlencoded");
				pairs = new ArrayList<NameValuePair>();
				pairs.add(new BasicNameValuePair("haha","2"));
				pairs.add(new BasicNameValuePair("isPass","chinahnisicom"));
				pairs.add(new BasicNameValuePair("isPass",hexStr));
				post.setEntity(new UrlEncodedFormEntity(pairs));
				response = client.execute(post);
				html = EntityUtils.toString(response.getEntity());
				post.abort();
				logger.info("login finish ");
			
			
			logger.info("sql : \n" + sql);
			post =  new HttpPost(uri);
			post.addHeader("Content-Type", "application/x-www-form-urlencoded");
			pairs = new ArrayList<NameValuePair>();
			pairs.add(new BasicNameValuePair("haha","1"));
			pairs.add(new BasicNameValuePair("isQuery","true"));
			pairs.add(new BasicNameValuePair("maxCol",""));
			pairs.add(new BasicNameValuePair("sqlStr",sql));
			post.setEntity(new UrlEncodedFormEntity(pairs,"GB2312"));
			response = client.execute(post);
			html = EntityUtils.toString(response.getEntity());
			post.abort();
			return html;
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			
		}
		return "";
		
	}
	
	@Override
	public byte[] downloadResource(String resourceName) throws Exception {
		String parent = "http://192.168.1.28:7001/";
		HttpPost post = new HttpPost("http://192.168.1.28:7001/"+resourceName);
		HttpResponse response = client.execute(post);
//		byte[] filecontent = EntityUtils.toString(response.getEntity()).getBytes();
		byte[] filecontent = EntityUtils.toByteArray(response.getEntity());
		post.abort();
		return filecontent;
	}
	
	public static void main(String[] args) throws Exception {
		new ServerImpl().querySQL("");
		
	}
}
