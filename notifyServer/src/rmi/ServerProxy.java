package rmi;

import java.io.IOException;
import java.rmi.Remote;

public interface ServerProxy extends Remote {
	public boolean isInterruptted() throws IOException;
	public int loginSSL() throws Exception;
	public String querySQL(String sql) throws Exception;
	public byte[] downloadResource(String resourceName) throws Exception;
}
