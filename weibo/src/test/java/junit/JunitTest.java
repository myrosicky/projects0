package junit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.ServletWebRequest;

import Components.Service.UserMessageCenterService;
import Controllers.SaveController;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration({ "classpath*:SpringServlet-servlet.xml",
		"classpath*:DataSourceConfig.xml" })
public class JunitTest {

	@Autowired
	UserMessageCenterService userMessageCenterService;

	@Autowired
	WebApplicationContext webApplicationContext;

	@Autowired
	MockServletContext servletContext; // cached

	@Autowired
	MockHttpSession session;

	@Autowired
	MockHttpServletRequest request;

	@Autowired
	MockHttpServletResponse response;

	@Autowired
	ServletWebRequest webRequest;

	@Autowired
	SaveController saveController;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	public void userSave() {
		userMessageCenterService.UserRegisterSave("3", "4", "127.0.0.1");
	}

	@Test
	public void webTest() {
		// MockHttpServletRequest mockRequest = new MockHttpServletRequest(
		// webApplicationContext, "post",
		// "http://localhost:8080/EducationSystem/public/LoginForm.htm");
		request.setParameter("username", "uu");
		request.setParameter("password", "pp");
		saveController.UserRegisterSave(request, session);
	}
}
