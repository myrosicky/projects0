<%@ page language="java" contentType="text/html; charset=GBK"
    pageEncoding="GBK"%>
<%@ taglib prefix="sprform" uri="http://www.springframework.org/tags/form"%>
<%@page import="Components.SessionUtil"%>
<%
	String contextPath = request.getContextPath();
	Object uID_obj = SessionUtil.getUID(request);
	int uID = uID_obj==null?-1:(Integer)uID_obj;
	
%>
<link rel="stylesheet" type="text/css" href="<%=contextPath %>/CSS/public/WBPublishForm.css">
<script type="text/javascript" src="<%=contextPath %>/JavaScript/public/jquery-1.7.2.js" ></script>
<script  type="text/javascript" src="<%=request.getContextPath() %>/dwr/engine.js"  ></script>
<script  type="text/javascript"  src="<%=request.getContextPath() %>/dwr/interface/WebBlogUtil.js"  ></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/dwr/util.js"></script>
<script type="text/javascript" src="<%=contextPath %>/JavaScript/public/WBPublishForm.js" ></script>

<div id="WBContent_div" class="">
	<form name="WBContent_form">
		<table>
			<tr><td>Write Something...</td></tr>
			<tr><td><textarea name="Content" ></textarea></td></tr>
			<tr><td>
			<a href="#" name="PUBLISH" class="publishbutton">publish</a>
			</td></tr>
		</table>
		<input type="hidden" name="Type" value="1" /> 
		<input type="hidden" name="uID" value="<%=uID %>"  />
	</form>
</div>
