package Filters;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.filter.AbstractRequestLoggingFilter;

public class MyAbstractRequestLoggingFilter extends
		AbstractRequestLoggingFilter {

	@Autowired
	private Components.Loggers.MyLogger myLogger;

	public MyAbstractRequestLoggingFilter() {
		this.setIncludeQueryString(true);
		this.setIncludeClientInfo(true);
	}

	@Override
	protected void afterRequest(HttpServletRequest arg0, String arg1) {
		// myLogger.log(arg1);
	}

	@Override
	protected void beforeRequest(HttpServletRequest arg0, String arg1) {
		// myLogger.log(arg1);
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request,
			HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {

		// myLogger.log("doFilterInternal");
		super.doFilterInternal(request, response, filterChain);
	}

}
