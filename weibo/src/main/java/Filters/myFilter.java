package Filters;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.filter.OncePerRequestFilter;

import Components.CookieUtil;

public class myFilter extends OncePerRequestFilter {

	public myFilter() {
		// TODO Auto-generated constructor stub

	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request,
			HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {

		if (!checkIfCookieIncludeObj(request.getCookies(),
				CookieUtil.serviceTicketName)) {
			/*
			 * redirecting to error page, but not change the url in users'
			 * explorer
			 */
			// request.getRequestDispatcher("/public/Fail.htm").forward(request,
			// response);

		}
		super.doFilter(request, response, filterChain);
	}

	/**
	 * check if there is existing Object in the cookie
	 * 
	 * @param cookies
	 * @param ObjName
	 *            object name
	 * @return boolean
	 */
	private boolean checkIfCookieIncludeObj(Cookie[] cookies, String ObjName) {
		int length = cookies.length;
		for (int i = 0; i < length; i++) {
			if (cookies[i].getName().equals(ObjName)) {
				return true;
			}
		}
		return false;
	}

}
