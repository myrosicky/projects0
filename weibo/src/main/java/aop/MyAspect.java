package aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class MyAspect {

	@Autowired
	private Components.Loggers.MyLogger logger;

	@Before("execution(* Components.DAO.common.iface.DBProxyInterface.*(..)) ")
	private void beginTransaction(JoinPoint joinPoint) {
		logger.log(joinPoint.getSignature().toString()
				+ " Database transaction begin!");
	}

	@AfterThrowing("execution(* Components.DAO.common.iface.DBProxyInterface.*(..)) ")
	private void endTransaction(JoinPoint joinPoint) {
		logger.log(joinPoint.getSignature().toString()
				+ " Database transaction end!");
	}

	@Around("execution(* Components.DAO.common.Hiber_DatabaseDao.*(..)) ")
	private void aroundTransaction(JoinPoint joinPoint) {
		logger.log(joinPoint.getSignature().toString() + " for cglib proxy");
	}

}
