package Components.repository;

import org.springframework.data.repository.CrudRepository;

import Components.DB.Buser;

public interface BuserRepository extends CrudRepository<Buser, Long> {
	public Buser findBuserByUsernameAndPassword(String username, String password);

	public Buser findBuserByUsername(String username);
}
