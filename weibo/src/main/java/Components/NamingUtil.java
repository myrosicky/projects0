package Components;

public class NamingUtil {

	/**
	 * current speaking user who send message
	 */
	public static String currentSUID = "userId_speak";
	/**
	 * current listening user who receive message
	 */
	public static String currentLUID = "userId_listen";

}
