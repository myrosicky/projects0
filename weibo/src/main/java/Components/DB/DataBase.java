package Components.DB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.tomcat.dbcp.dbcp.BasicDataSource;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DataBase {

	static BasicDataSource bds = null;

	@Autowired
	private Components.Loggers.MyLogger logger;

	// static definition start
	{
		SAXReader reader = new SAXReader();
		Document doc = null;
		String DB_DriverName = "", username = "", password = "", DB_URL = "", DB_Name = "";
		String dbConfigFile = "config.xml", DataBaseVendor = "Oracle";

		bds = new BasicDataSource();
		try {

			doc = reader.read(DataBase.class.getClassLoader()
					.getResourceAsStream(dbConfigFile));

			// 从配置文件中读取数据库模块--开始
			Node DB_Module = doc.selectSingleNode("DB_Modules")
					.selectSingleNode(DataBaseVendor + "_" + "DB_Module");
			DB_DriverName = DB_Module.selectSingleNode("DB_DriverName")
					.getText();
			username = DB_Module.selectSingleNode("username").getText();
			password = DB_Module.selectSingleNode("password").getText();
			DB_URL = DB_Module.selectSingleNode("DB_URL").getText();
			DB_Name = DB_Module.selectSingleNode("DB_Name").getText();
			// 从配置文件中读取数据库模块--结束

			// 根据以上数据，设置数据源属性值
			bds.setDriverClassName(DB_DriverName);
			bds.setUrl(DB_URL + DB_Name);
			bds.setUsername(username);
			bds.setPassword(password);
			bds.setMaxActive(10);

		} catch (DocumentException e1) {
			logger.log(e1.getMessage());
		}

	}

	// static definition end

	public Connection getConnection() {
		try {
			return bds.getConnection();
		} catch (SQLException e) {
			logger.log(e.getMessage());
			return null;
		}
	}

	/**
	 * <b><font color=red>Query and return the result set list</font></b>
	 * 
	 * @param sql
	 * @param parameterObjects
	 * @return
	 */
	public List<Map> queryFormapList(String sql, Object[] parameterObjects) {
		return queryFormapList(sql, parameterObjects, null);
	}

	public List<Map> queryFormapList(String sql, Object[] parameterObjects,
			Connection connection) {
		boolean isSuccess = false;
		List<Map> result_list = null;
		Map<String, Object> row_map = null;
		Connection con = connection == null ? getConnection() : connection;
		if (con == null)
			return null;
		try {
			result_list = new ArrayList<Map>();
			con.setAutoCommit(false);
			PreparedStatement ps = con.prepareStatement(sql);

			PStatementConstruct(ps, parameterObjects);

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				row_map = new HashMap<String, Object>();

				ResultSetMetaData rsmd = rs.getMetaData();
				int columnCount = rsmd.getColumnCount();
				for (int i = 1; i < columnCount + 1; i++) {
					row_map.put(rsmd.getColumnName(i), rs.getObject(i));
				}

				result_list.add(row_map);
			}
			isSuccess = true;
		} catch (SQLException e) {
			logger.log(e.getMessage());
		} catch (IllegalArgumentException e) {
			logger.log(e.getMessage());
		} catch (SecurityException e) {
			logger.log(e.getMessage());
		}
		if (isSuccess)
			return result_list;
		else
			return null;

	}

	/**
	 * executing the sql and return operating result.
	 * 
	 * @param sql_exe
	 * @return boolean
	 */

	public boolean execute(String sql, Object[] parameterObjects) {
		return execute(sql, parameterObjects, null);
	}

	public boolean execute(String sql, Object[] parameterObjects,
			Connection connection) {
		boolean isSuccess = false;
		Connection conn = connection == null ? getConnection() : connection;

		try {
			conn.setAutoCommit(false);
			PreparedStatement ps = conn.prepareStatement(sql);
			PStatementConstruct(ps, parameterObjects);
			ps.execute();
			conn.commit();
			isSuccess = true;
		} catch (SQLException e) {
			logger.log(e.getMessage());
			isSuccess = false;
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				logger.log(e.getMessage());
			}
		}
		return isSuccess;
	}

	/**
	 * <b><font color=red>replace query placeholder with input
	 * parameters</font></b>
	 * 
	 * @param ps
	 * @param parameterObjects
	 * @throws SQLException
	 */
	private static void PStatementConstruct(PreparedStatement ps,
			Object[] parameterObjects) throws SQLException {
		int length = parameterObjects == null ? 0 : parameterObjects.length;
		for (int i = 0; i < length; i++) {
			ps.setObject(i + 1, parameterObjects[i]);
		}
	}
}
