package Components.DB;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "BWEBBLOG")
public class Bwebblog {
	int Wb_id, Buser_id, readTIme, type, Pre_Wb_id, valid;
	String Content;
	Date pDate;

	@Id
	public int getWb_id() {
		return Wb_id;
	}

	@Column
	public int getBuser_id() {
		return Buser_id;
	}

	@Column
	public int getReadTIme() {
		return readTIme;
	}

	@Column
	public int getType() {
		return type;
	}

	@Column
	public int getPre_Wb_id() {
		return Pre_Wb_id;
	}

	@Column
	public int getValid() {
		return valid;
	}

	@Column
	public String getContent() {
		return Content;
	}

	@Column
	public Date getpDate() {
		return pDate;
	}

	public void setWb_id(int wb_id) {
		Wb_id = wb_id;
	}

	public void setBuser_id(int buser_id) {
		Buser_id = buser_id;
	}

	public void setReadTIme(int readTIme) {
		this.readTIme = readTIme;
	}

	public void setType(int type) {
		this.type = type;
	}

	public void setPre_Wb_id(int pre_Wb_id) {
		Pre_Wb_id = pre_Wb_id;
	}

	public void setValid(int valid) {
		this.valid = valid;
	}

	public void setContent(String content) {
		if (content != null)
			Content = content;
	}

	public void setpDate(Date pDate) {
		if (pDate != null)
			this.pDate = pDate;
	}

}
