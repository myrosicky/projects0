package Components.DB;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * the persistence class of Buser Table
 * 
 * @author liangli
 * 
 */

@Entity
@Table
public class Buser implements Serializable {
	private static final long serialVersionUID = 1L;

	private int Buser_id, valid;
	private String username, password, general_ip, remark;
	private Date register_date, loginLastTime;

	@Id
	public int getBuser_id() {
		return Buser_id;
	}

	public void setBuser_id(int buser_id) {
		Buser_id = buser_id;
	}

	@Column(name = "VALID", insertable = false)
	public int getValid() {
		return valid;
	}

	public void setValid(int valid) {
		this.valid = valid;
	}

	@Column(name = "USERNAME")
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Column(name = "PASSWORD")
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "GENERAL_IP")
	public String getGeneral_ip() {
		return general_ip;
	}

	public void setGeneral_ip(String general_ip) {
		this.general_ip = general_ip;
	}

	@Column(name = "REMARK")
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Column(name = "REGISTER_DATE", updatable = false)
	public Date getRegister_date() {
		return register_date;
	}

	public void setRegister_date(Date register_date) {
		this.register_date = register_date;
	}

	@Column(name = "LOGINLASTTIME", columnDefinition = "default sysdate")
	public Date getLoginLastTime() {
		return loginLastTime;
	}

	public void setLoginLastTime(Date loginLastTime) {
		if (loginLastTime != null)
			this.loginLastTime = loginLastTime;
	}

}
