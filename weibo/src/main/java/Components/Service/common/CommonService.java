package Components.Service.common;

import org.springframework.stereotype.Service;

import Components.Service.iface.ServiceProxyInterface;

@Service
public class CommonService implements ServiceProxyInterface {

	public void serviceMethos() {
		System.err.println(this.getClass().getName());
	}
}
