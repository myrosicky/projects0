package Components.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Components.DAO.common.iface.DBProxyInterface;
import Components.DB.Buser;
import Components.Loggers.MyLogger;
import Components.Service.iface.ServiceProxyInterface;
import Components.repository.BuserRepository;

@Service
public class UserMessageCenterService {

	@Autowired
	DBProxyInterface dbProxyInterface;

	@Autowired
	ServiceProxyInterface commonService;

	private Components.Loggers.MyLogger logger = new MyLogger();

	@Autowired
	private BuserRepository buserRepository;

	/**
	 * check if the user existing.if so,just return a false signal.otherwise do
	 * the inserting operation and return a true signal.
	 * 
	 * @param username
	 * @param password
	 * @param remoteHost
	 * @return boolean
	 */
	public boolean UserRegisterSave(String username, String password,
			String remoteHost) {
		// check if username exists
		Buser buser = null;
		try {
			buser = buserRepository.findBuserByUsername(username);
		} catch (Exception e1) {

		}
		if (buser != null) {
			logger.log(remoteHost + " register fail: username(" + username
					+ ") exists");
			return false;
		}

		// if user not exists, add it
		buser = new Buser();
		buser.setBuser_id(2);
		buser.setUsername(username);
		buser.setPassword(password);
		buser.setGeneral_ip(remoteHost);
		try {
			buserRepository.save(buser);
		} catch (Exception e) {
			logger.log(e.getLocalizedMessage());
			return false;
		}
		return true;
	}

	public boolean login(String username, String password) {
		boolean isLoginSuccess = false;
		Buser buser = buserRepository.findBuserByUsernameAndPassword(username,
				password);
		if (buser != null) {
			isLoginSuccess = true;
		}
		return isLoginSuccess;
	}

}
