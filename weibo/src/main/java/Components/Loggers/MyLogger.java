package Components.Loggers;

import org.springframework.stereotype.Component;

@Component
public class MyLogger {

	public void log(String message) {
		System.err.println("###########" + message);
	}

}
