package Components;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import Components.DB.DataBase;

@Component
public class WBMessageCenter {

	@Autowired
	private DataBase dataBase;

	/**
	 * <b><font color=red>return unread webblogs according to the requesting
	 * user </font></b>
	 * 
	 * @param Buser_id
	 * @return
	 */
	public String returnUnreadWebBlog(int Buser_id) {

		List<Map> newWBList = null;
		StringBuilder sbBuilder = new StringBuilder();
		VFBuilder vfBuilder = new VFBuilder();
		newWBList = dataBase
				.queryFormapList(OperateStatement.queryComprehenceWB,
						new Integer[] { Buser_id });
		sbBuilder.append(vfBuilder.writeWBContentInHtml(newWBList));

		return sbBuilder.toString();
	}
}
