package Components;

import javax.servlet.http.HttpServletRequest;

public class RequestUtil {

	public static String getParamFromReq(HttpServletRequest request,
			String paramName) {
		return convertNull2EmptyStr(request.getParameter(paramName));
	}

	public static String convertNull2EmptyStr(String str) {
		return str == null || str.equalsIgnoreCase("null") ? "" : str;
	}
}
