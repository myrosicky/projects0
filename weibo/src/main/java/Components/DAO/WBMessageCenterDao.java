package Components.DAO;

import java.sql.Connection;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import Components.DB.Bwebblog;

@Repository
public class WBMessageCenterDao {

	@Autowired
	private SessionFactory hibernateSessionFactory;

	/**
	 * <b><font color=red>insert record in bwebblog table </font></b>
	 * 
	 * @param buser_Id
	 * @param content
	 * @param type
	 * @param pre_Wb_Id
	 * @param connection
	 *            used for transaction control. if don't needed,assign null
	 * @return
	 */
	public boolean writeWebBlog(int buser_id, String content, int type,
			int pre_Wb_id, Connection connection) {
		boolean isSuccess = false;
		Bwebblog new_wb = new Bwebblog();
		new_wb.setBuser_id(buser_id);
		new_wb.setContent(content);
		new_wb.setType(type);
		if (pre_Wb_id != -1) {
			new_wb.setPre_Wb_id(pre_Wb_id);
		}

		Session session = this.hibernateSessionFactory.getCurrentSession();
		Transaction transaction = session.beginTransaction();

		// webblog save transaction
		transaction.begin();
		session.save(new_wb);
		transaction.commit();

		isSuccess = true;
		return isSuccess;
	}

}
