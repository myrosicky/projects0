package Components.DAO.common;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import Components.DAO.common.iface.DBProxyInterface;

@Repository("dbProxyInterface")
public class Hiber_DatabaseDao implements DBProxyInterface {

	@Autowired
	private SessionFactory sessionFactory;

	/**
	 * check if the specific user existing according to the input parameters.<br/>
	 * 
	 * @param Buser_id
	 * @return Buser
	 * 
	 *         public Buser queryUserFrom(int Buser_id) { Buser buser = null;
	 *         Session session =
	 *         this.hibernateTransactionManager.getSessionFactory()
	 *         .openSession(); Query query = session
	 *         .createQuery("from Buser t where t.BUSER_ID=? and t.valid=1 ");
	 *         query.setBigDecimal(1, new BigDecimal(Buser_id));
	 * 
	 *         List<Buser> list = query.list(); if (!(list.isEmpty() ||
	 *         list.size() > 1)) { buser = list.get(0); } return buser; }
	 */

	public Boolean save(Object obj) throws Exception {
		boolean isSuccess = false;
		Session session = null;
		session = sessionFactory.getCurrentSession();
		session.saveOrUpdate(obj);
		isSuccess = true;
		return isSuccess;
	}

	public List<Object> query(String hqlString, Object[] objects) {
		Object rtnObj = null;
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(hqlString);
		int length = objects.length;
		for (int j = 0; j < length; j++) {
			query.setParameter(j, objects[j]);
		}
		List<Object> list = query.list();
		return list;
	}

}
