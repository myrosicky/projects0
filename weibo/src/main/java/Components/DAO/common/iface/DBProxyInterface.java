package Components.DAO.common.iface;

import java.util.List;

import org.springframework.stereotype.Component;

@Component
public interface DBProxyInterface {

	public Boolean save(Object obj) throws Exception;

	public List<Object> query(String hqlString, Object[] objects)
			throws Exception;

}
