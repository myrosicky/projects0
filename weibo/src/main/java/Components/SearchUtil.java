package Components;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.LongField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.springframework.stereotype.Component;

@Component
public class SearchUtil {

	private String resourcesPath = "d:/reosurces";
	private final Version currentVersion = Version.LUCENE_4_10_1;
	private final String f_PATH = "path";
	private final String f_CONTENTS = "contents";
	private final String f_MODIFYDATE = "modifydate";

	public void indexSources(String str_fileDir) throws IOException {
		boolean create = false;
		try {
			create = searchResources(f_PATH,
					str_fileDir.substring(str_fileDir.indexOf(":") + 1)) == null;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Directory dir = FSDirectory.open(new File(resourcesPath));
		Analyzer analyzer = new StandardAnalyzer();
		IndexWriterConfig iwc = new IndexWriterConfig(currentVersion, analyzer);
		if (create) {
			// Create a new index in the directory, removing any
			// previously indexed documents:
			iwc.setOpenMode(OpenMode.CREATE);
		} else {
			// Add new documents to an existing index:
			iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);
		}

		// Optional: for better indexing performance, if you
		// are indexing many documents, increase the RAM
		// buffer. But if you do this, increase the max heap
		// size to the JVM (eg add -Xmx512m or -Xmx1g):
		//
		// iwc.setRAMBufferSizeMB(256.0);

		IndexWriter writer = new IndexWriter(dir, iwc);
		IndexFiles(writer, new File(str_fileDir));
		writer.close();
	}

	private void IndexFiles(IndexWriter writer, File fileDir)
			throws IOException {
		Document doc = new Document();
		FileInputStream fis = new FileInputStream(fileDir);

		doc.add(new StringField(f_PATH, fileDir.getPath(), Field.Store.YES));
		doc.add(new LongField(f_MODIFYDATE, fileDir.lastModified(),
				Field.Store.NO));
		doc.add(new TextField(f_CONTENTS, new BufferedReader(
				new InputStreamReader(fis))));

		for (IndexableField field : doc.getFields()) {
			System.out.println(field.toString());
		}
		if (writer.getConfig().getOpenMode() == OpenMode.CREATE) {
			writer.addDocument(doc);
		} else {
			writer.updateDocument(new Term(f_PATH, fileDir.getPath()), doc);
		}
		fis.close();
	}

	public List<String> searchResources(String field, String searchTerm)
			throws ParseException, IOException {
		IndexReader indexReader = null;
		try {
			indexReader = DirectoryReader.open(FSDirectory.open(new File(
					resourcesPath)));
		} catch (IOException e) {
			System.out
					.println("SearchUtil.searchResources():" + e.getMessage());
			return null;
		}

		IndexSearcher searcher = new IndexSearcher(indexReader);
		Analyzer analyzer = new StandardAnalyzer();
		QueryParser parser = new QueryParser(field, analyzer);
		Query query = parser.parse(searchTerm);
		TopDocs results = searcher.search(query, null, 2);
		ScoreDoc[] hits = results.scoreDocs;
		int numTotalHits = results.totalHits;
		int start = 0;
		int end = Math.min(hits.length, 10);
		System.out.println("term: " + query.toString() + "\n\t total hits:"
				+ numTotalHits);
		for (int i = start; i < end; i++) {
			System.err.println(searcher.doc(hits[i].doc).toString());
		}
		indexReader.close();
		return null;
	}

	public List<String> searchResources(String searchTerm)
			throws ParseException, IOException {
		List<String> result = new ArrayList<String>();
		try {
			result.addAll(searchResources(f_CONTENTS, searchTerm));
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			result.addAll(searchResources(f_PATH, searchTerm));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;

	}

	public static void main(String[] args) {
		SearchUtil searchUtil = new SearchUtil();
		try {
			searchUtil.indexSources("E:\\opInfo.txt");
			searchUtil.indexSources("E:\\opInfo2.txt");
			searchUtil.searchResources("home");
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
