package Components;

import javax.servlet.http.HttpServletRequest;

public class SessionUtil {

	public static Object getUID(HttpServletRequest request) {
		return request.getSession(false).getAttribute(NamingUtil.currentSUID) == null ? 0
				: request.getSession(false)
						.getAttribute(NamingUtil.currentSUID);
	}

	public static void generateNewUID(HttpServletRequest request) {
		request.getSession(false).setAttribute(NamingUtil.currentSUID, 1);
	}
}
