package rmi;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class MyRemoteClass implements MyRemoteInterface {

	public String[] sayYourName(String name) throws RemoteException {
		System.err.println("remote reference");
		return new String[] { "kick", name };
	}

	public static void main(String[] args) {
		try {
			MyRemoteClass myRemoteClass = new MyRemoteClass();
			MyRemoteInterface myRemoteInterface = (MyRemoteInterface) UnicastRemoteObject
					.exportObject(myRemoteClass, 0);
			Registry registry = LocateRegistry.getRegistry();
			registry.rebind("myRemoteInterface", myRemoteInterface);
			System.err.println("system ready!!");
		} catch (RemoteException e) {
			e.printStackTrace();
		}

	}

	public Boolean checkIfSuccess() throws RemoteException {
		return true;
	}
}
