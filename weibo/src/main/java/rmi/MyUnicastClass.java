package rmi;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class MyUnicastClass extends UnicastRemoteObject {

	protected MyUnicastClass() throws RemoteException {
		super();
	}

}
