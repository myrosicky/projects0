package rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface MyRemoteInterface extends Remote {
	public Boolean checkIfSuccess() throws RemoteException;

	String[] sayYourName(String name) throws RemoteException;

}
