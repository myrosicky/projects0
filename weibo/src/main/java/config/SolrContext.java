package config;

import java.net.MalformedURLException;

import javax.annotation.Resource;

import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;

//@Configuration
//@EnableSolrRepositories(basePackages = { "Components.repository" }, multicoreSupport = true)
class SolrContext {
	private @Resource Environment env;

	@Bean
	public HttpSolrServer solrServer() throws MalformedURLException,
			IllegalStateException {
		return new HttpSolrServer(env.getRequiredProperty("solr.host"));
	}
}
