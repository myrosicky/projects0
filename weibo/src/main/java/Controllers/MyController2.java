package Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * mvc Controller 2
 * apply to url which matches pattern "contextPath/chat"
 * @author Administrator
 * 
 */
@Controller
@RequestMapping("/chat")
public class MyController2{

	@RequestMapping("/talk.htm")
	public String talk(@RequestParam(value="name",required=false,defaultValue="gaga")String name, Model model){
		String msg = "hello,"+name;
		model.addAttribute("name", name);
		return "Page_Add_Info_Base";
	}
}
