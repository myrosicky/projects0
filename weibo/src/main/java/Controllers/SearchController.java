package Controllers;

import java.io.IOException;
import java.util.List;

import org.apache.lucene.queryparser.classic.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import Components.SearchUtil;

@Controller
public class SearchController {

	@Autowired
	SearchUtil searchutil;

	@RequestMapping("/search.do")
	public void search(@RequestParam("searchTerm") String searchTerm) {
		try {
			List<String> rtnList = searchutil.searchResources(searchTerm);
			for (String string : rtnList) {
				System.err.println(string);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
