package Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * mvc Controller apply to url which matches pattern "contextPath/"
 * 
 * @author Administrator
 * 
 */
@Controller
public class MyController {

	@Autowired
	private Components.Loggers.MyLogger logger;

	/**
	 * process whole url ends with ".htm"
	 * 
	 * @param jspPageName
	 *            the name of jsp file existing in web-inf/webpages/ folder
	 * @param model
	 * @return
	 */
	@RequestMapping("/{jspPageName}.htm")
	public String gotoPage(
			@PathVariable(value = "jspPageName") String jspPageName) {
		return jspPageName;
	}

	@RequestMapping("/public/{jspPageName}.htm")
	public String gotoPublicPage(
			@PathVariable(value = "jspPageName") String jspPageName) {
		return "public/" + jspPageName;
	}

	@RequestMapping("/mod_{moduleName}/{jspPageName}.htm")
	public String gotoModPage(
			@PathVariable(value = "moduleName") String moduleName,
			@PathVariable(value = "jspPageName") String jspPageName) {
		logger.log("module[" + moduleName + "]  is visiting");
		return "mod_" + moduleName + "/" + jspPageName;
	}

}
