package org.ll;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;
import java.util.Properties;

/**
 * a packet instance
 * 
 * @author liangli90432
 * @createTime 2014-9-24
 * @modifier
 * @modifyDescription
 * @see
 */
public class MyUCObject{

	private int connectTimeout = 20000;
	private String urlString;
	private URL url;
	private URLConnection urlConnection;
	
	private String content;
	private Properties headers;
	private boolean isReadyForSend;
	private int sendCnt;

	public int getSendCnt() {
		return sendCnt;
	}

	public void setSendCnt(int sendCnt) {
		this.sendCnt = sendCnt;
	}

	public Properties getHeaders() {
		return headers;
	}

	public void setHeaders(Properties headers) {
		this.headers = headers;
	}

	public int getConnectTimeout() {
		return connectTimeout;
	}

	public void setConnectTimeout(int connectTimeout) {
		this.connectTimeout = connectTimeout;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getUrlString() {
		return urlString;
	}

	public void setUrlString(String urlString) throws MalformedURLException,
			IOException {
		this.urlString = urlString;
		url = new URL(urlString);
		// regainConnection();
	}

	public void addPropertyToURLConn(Map<String, Object> headers)
			throws IOException {
		try {
			for (Object key : headers.keySet()) {
				urlConnection.addRequestProperty(String.valueOf(key),
						String.valueOf(headers.get(key)));
			}
		} catch (IllegalStateException e) {
			System.err.println("已连接，不能添加参数");
		}
	}

	public void send() {
		try {
			// urlConnection.getInputStream();
			regainConnection();
			urlConnection.connect();
		} catch (Exception e) {
			System.err.println("连接目标网站出错：" + e.getMessage());
		}
		try {
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
					urlConnection.getOutputStream()));
			bw.write(content);
			bw.flush();
			urlConnection.getOutputStream().close();
		} catch (Exception e) {
			System.err.println("连接目标网站出错：" + e.getMessage());
		}

	}

	public void regainConnection() throws IOException {
		urlConnection = url.openConnection();
		urlConnection.setConnectTimeout(connectTimeout);
		urlConnection.setDoOutput(true);
		urlConnection.setUseCaches(false);
		urlConnection.setAllowUserInteraction(true);
		

	}

	public void printResponseTo(OutputStream os) throws IOException {
		PrintWriter pw = new PrintWriter(os);
		String charSet = null;
		InputStream is = urlConnection.getInputStream();
		try {
			charSet = urlConnection.getContentType().split("charset=")[1];
		} catch (Exception e) {
			charSet = "UTF-8";
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(is,
				charSet));
		String line = "";
		while ((line = br.readLine()) != null) {
			pw.println(line);
		}
		pw.flush();
		pw.close();
		is.close();
		// regainConnection();
	}

	public void setReadyForSend(boolean isReadyForSend) {
		this.isReadyForSend = isReadyForSend;
	}

	public boolean isReadyForSend() {
		return isReadyForSend;
	}
	
	
	
}
