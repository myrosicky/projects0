package org.ll;

import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.ll.iface.Packet;

public class PacketGetImpl implements Packet {

	
	private HttpGet request;
	private URIBuilder uriBuilder;
	private HttpResponse response;
	private List<NameValuePair> pairs = new ArrayList<NameValuePair>();
	

	public HttpGet getRequest() {
		try {
			request.setURI(uriBuilder.build());
			return request;
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void setRequest(HttpGet request) {
		this.request = request;
	}

	public HttpResponse getResponse() {
		return response;
	}

	public void setResponse(HttpResponse response) {
		this.response = response;
	}

	@Override
	public boolean isReadyForSend() {
		return true;
	}

	@Override
	public void printResponseTo(OutputStream os) throws Exception {
		//os.println(EntityUtils.toString(response.getEntity()));
	}

	@Override
	public String getUrlString() {
		
		return null;
	}

	@Override
	public void setUrlString(String url) {
			try {
				uriBuilder = new URIBuilder(url);
				request = new HttpGet();	
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
	}

	public PacketGetImpl(String uri) throws URISyntaxException {
		super();
		setUrlString(uri);
	}
	
	@Override
	public void setContent(String content) {
		
	}
	
	public void addHeader(String name,String value) {
		request.addHeader(name, value);
	}
	
	@Override
	public void addParams(String name,String value) throws UnsupportedEncodingException {
		pairs.add(new BasicNameValuePair(name, value));
		uriBuilder.addParameters(pairs);
	}

	@Override
	public void setHeaders(Properties properties) {
		for (Entry<Object, Object> entry : properties.entrySet()) {
			request.addHeader((String)entry.getKey(), (String)entry.getValue());
		}
	}

	@Override
	public void setReadyForSend(boolean boolean1) {
	}
	
	public void setUrlEncodedFormEntity(Properties properties) throws UnsupportedEncodingException {
		for (Entry<Object, Object> entry : properties.entrySet()) {
			pairs.add(new BasicNameValuePair((String)entry.getKey(), (String)entry.getValue()));
		}
		uriBuilder.addParameters(pairs);
	}

}
