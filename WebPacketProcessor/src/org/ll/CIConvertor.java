package org.ll;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.ll.iface.Packet;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

/**
 * configuration info convertor
 * 
 * @author liangli90432
 * @createTime 2014-9-23
 * @modifier
 * @modifyDescription
 * @see
 */
public class CIConvertor {

	final static String packetDefineFilePath = "config/packet.xml";
	private static List<Packet> packetList = null;

	@SuppressWarnings("unchecked")
	public static List<Packet> getDefinedPackets() {
		if (packetList != null) {
			return packetList;
		}
		try {
			XStream xStream = new XStream(new DomDriver());
			InputStream is = new FileInputStream(new File(packetDefineFilePath));
			packetList = (List<Packet>) xStream.fromXML(is);
			is.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return packetList;
	}

	public static boolean hasDefinedPackets() {
		return !(getDefinedPackets() == null || getDefinedPackets().isEmpty());
	}

	public static void setDefinedPackets(List<Packet> newPacketList) {
		try {
			XStream xStream = new XStream(new DomDriver());
			FileOutputStream os = new FileOutputStream(new File(
					packetDefineFilePath));
			xStream.toXML(newPacketList, os);
			os.flush();
			os.close();
			packetList = newPacketList;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
