/*
 * MainFrame.java
 *
 * Created on __DATE__, __TIME__
 */

package org.ll;

import java.awt.event.ContainerAdapter;
import java.awt.event.ContainerEvent;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.table.DefaultTableModel;

import org.ll.iface.Packet;

/**
 *
 * @author __USER__
 */
public class MainFrame extends javax.swing.JFrame {
	private AddPacketFrame apFrame;

	private WebPacketProcessor wpp = null;

	private String[] tableTitle = { "no", "urlString", "connectTimeout" };
	private DefaultTableModel dataModel = new DefaultTableModel(
			new Vector<String>(), new Vector<String>(Arrays.asList(tableTitle))) {

		@Override
		public Class<?> getColumnClass(int columnIndex) {
			return super.getColumnClass(columnIndex);
		}

	};

	/** Creates new form MainFrame */
	public MainFrame() {
		wpp = new WebPacketProcessor();
		initComponents();

	}

	// GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jScrollPane1 = new javax.swing.JScrollPane();
		jScrollPane1.addContainerListener(new ContainerAdapter() {
			@Override
			public void componentAdded(ContainerEvent arg0) {
				refreshTable();
			}
		});
		jtb_packets = new javax.swing.JTable();
		jtb_packets.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		add = new javax.swing.JButton();
		send = new javax.swing.JButton();
		jb_refresh = new javax.swing.JButton();

		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		setTitle("main frame");
		setAlwaysOnTop(true);

		jtb_packets.setModel(dataModel);
		jtb_packets.setToolTipText("packets table");
		jScrollPane1.setViewportView(jtb_packets);

		add.setText("add");
		add.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				addActionPerformed(evt);
			}
		});

		send.setText("send");
		send.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				sendActionPerformed(evt);
			}
		});

		jb_refresh.setText("refresh");
		jb_refresh.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jb_refreshActionPerformed(evt);
			}
		});

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		layout.setHorizontalGroup(layout
				.createParallelGroup(Alignment.LEADING)
				.addGroup(
						layout.createSequentialGroup()
								.addGap(58)
								.addComponent(add)
								.addPreferredGap(ComponentPlacement.RELATED,
										213, Short.MAX_VALUE)
								.addComponent(send).addGap(98))
				.addGroup(
						layout.createSequentialGroup()
								.addGap(25)
								.addComponent(jScrollPane1,
										GroupLayout.PREFERRED_SIZE, 385,
										GroupLayout.PREFERRED_SIZE)
								.addContainerGap(67, Short.MAX_VALUE))
				.addGroup(
						layout.createSequentialGroup().addGap(181)
								.addComponent(jb_refresh)
								.addContainerGap(221, Short.MAX_VALUE)));
		layout.setVerticalGroup(layout.createParallelGroup(Alignment.LEADING)
				.addGroup(
						layout.createSequentialGroup()
								.addGap(38)
								.addComponent(jScrollPane1,
										GroupLayout.PREFERRED_SIZE, 122,
										GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.UNRELATED)
								.addComponent(jb_refresh)
								.addGap(44)
								.addGroup(
										layout.createParallelGroup(
												Alignment.BASELINE)
												.addComponent(add)
												.addComponent(send))
								.addContainerGap(48, Short.MAX_VALUE)));
		getContentPane().setLayout(layout);

		pack();
	}// </editor-fold>
		// GEN-END:initComponents

	private void sendActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		try {
			int[] selection = jtb_packets.getSelectedRows();

			for (int i = 0; i < selection.length; i++) {
				selection[i] = jtb_packets.convertRowIndexToModel(selection[i]);
				wpp.sendPacketAsIE(CIConvertor.getDefinedPackets().get(
						selection[i]));
			}

			JOptionPane.showMessageDialog(null, "send success");
		} catch (Exception e1) {
			e1.printStackTrace();
			JOptionPane.showMessageDialog(null, e1.getMessage());
		}
	}

	private void jb_refreshActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

		refreshTable();
	}

	private void addActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		try {
			apFrame.setVisible(true);
		} catch (Exception e) {
			apFrame = new AddPacketFrame();
			apFrame.setVisible(true);
		}
	}

	private void refreshTable() {
		List<Packet> packetList = CIConvertor.getDefinedPackets();
		Vector<Vector<String>> vector = new Vector<Vector<String>>();
		int rowIndex = 0;

		// rows enumeration
		try {
			for (Packet packet : packetList) {
				Vector<String> vector2 = new Vector<String>();
				vector2.add(++rowIndex + "");

				// columns enumeration
				int tableTitleLength = tableTitle.length;
				for (String title : tableTitle) {
					String getMethodName = "get"
							+ title.replaceFirst(title.substring(0, 1), title
									.substring(0, 1).toUpperCase());
					try {
						vector2.add(String.valueOf(packet.getClass()
								.getDeclaredMethod(getMethodName, null)
								.invoke(packet, null)));
					} catch (NoSuchMethodException e) {
						System.err.println("找不到该列定义的方法" + getMethodName);
					}
				}
				vector.add(vector2);
			}
			dataModel.setDataVector(vector,
					new Vector<String>(Arrays.asList(tableTitle)));

			jtb_packets.setModel(dataModel);
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}

	}

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new MainFrame().setVisible(true);
			}
		});
	}

	// GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JButton add;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JButton jb_refresh;
	private javax.swing.JTable jtb_packets;
	private javax.swing.JButton send;
	// End of variables declaration//GEN-END:variables

}