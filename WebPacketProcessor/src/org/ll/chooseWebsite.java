package org.ll;

import java.awt.CardLayout;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.eclipse.wb.swing.FocusTraversalOnArray;
import org.ll.iface.WebSite;

public class chooseWebsite {

	private ArrayList<WebSite> webSites;
	private JFrame frame;
	private JTextField txtWebsite;
	private JTextField txttxt;
	private final String configFile = "bin/websites.properties";

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					chooseWebsite window = new chooseWebsite();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * 
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public chooseWebsite() throws FileNotFoundException, IOException {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * 
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	private void initialize() throws FileNotFoundException, IOException {

		webSites = new ArrayList<WebSite>();
//		for (int i=0;true;i++) {
//			webSites.add(e)properties.getProperty("ws"+i+".name");
//		}
		
		frame = new JFrame();
		frame.setBounds(100, 100, 507, 350);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new CardLayout(0, 0));

		JDesktopPane desktopPane = new JDesktopPane();
		frame.getContentPane().add(desktopPane, "name_1817691943876");

		final JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 0, 491, 263);
		desktopPane.add(tabbedPane);

		JPanel panel = new JPanel();
		panel.setToolTipText("\u767E\u59D3\u7F51");
		tabbedPane.addTab("\u767E\u59D3\u7F51", null, panel,
				"\u767E\u59D3\u7F51");
		tabbedPane.setDisplayedMnemonicIndexAt(0, 0);
		panel.setLayout(null);

		JButton btnNewButton = new JButton("New button");
		btnNewButton.setBounds(75, 201, 93, 23);
		panel.add(btnNewButton);

		txtWebsite = new JTextField();
		txtWebsite.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				WebSite webSite = webSites.get(tabbedPane.getSelectedIndex());
				webSite.setUrl(txtWebsite.getText());
			}
		});
		txtWebsite.setToolTipText("\u767E\u59D3\u7F51\u7F51\u5740");
		txtWebsite.setText("http://taiyuan.baixing.com/");
		txtWebsite.setBounds(37, 10, 401, 21);
		panel.add(txtWebsite);
		txtWebsite.setColumns(10);

		JLabel lblNewLabel = new JLabel("\u7F51\u5740");
		lblNewLabel.setBounds(10, 13, 54, 15);
		panel.add(lblNewLabel);

		JLabel lbltxt = new JLabel(
				"\u5185\u5BB9\uFF1A\uFF08\u9009\u62E9txt\u6587\u4EF6\uFF09");
		lbltxt.setBounds(10, 53, 170, 15);
		panel.add(lbltxt);

		txttxt = new JTextField();
		txttxt.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				WebSite webSite = webSites.get(tabbedPane.getSelectedIndex());
				JFileChooser fileChooser = new JFileChooser(new File(webSite
						.getContentFilePath()).getParentFile());
				FileNameExtensionFilter filter = new FileNameExtensionFilter(
						"txt, doc, xls", "txt", "doc", "xls");
				fileChooser.setFileFilter(filter);
				int returnVal = fileChooser.showDialog(null, "ѡ��");
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File selected = fileChooser.getSelectedFile();
					webSite.setContentFilePath(selected.getAbsolutePath());
					txttxt.setText(fileChooser.getSelectedFile()
							.getAbsolutePath());
				}
			}
		});
		txttxt.setEditable(false);
		txttxt.setText("\u70B9\u51FB\u9009\u62E9txt\u6587\u4EF6");
		txttxt.setBounds(10, 78, 428, 21);
		panel.add(txttxt);
		txttxt.setColumns(10);
		panel.setFocusTraversalPolicy(new FocusTraversalOnArray(
				new Component[] { lblNewLabel, txtWebsite, lbltxt, txttxt,
						btnNewButton }));
	}
}
