package org.ll.modal;

import java.io.File;

import org.apache.http.util.EntityUtils;
import org.ll.PacketGetImpl;
import org.ll.WebPacketProcessor;
import org.ll.iface.WebSite;

public class Ganji extends WebSite {

	public Ganji(String name, String url, File contentFile) {
		super("�ϼ���", "http://gz.ganji.com/", "");
	}
	
	public Ganji() {
		super("�ϼ���", "http://gz.ganji.com/", "");
	}

	@Override
	public void execute() throws Exception {
		WebPacketProcessor wpp = new WebPacketProcessor();
		PacketGetImpl get = null;
		String username = "hlp11";
		try {
			/******
			 * normal visit
			 */
			get = new PacketGetImpl(getUrl());
			get.setResponse(wpp.execute(get));
			System.err.println(EntityUtils.toString(get.getResponse().getEntity()));
			get.getRequest().abort();
			
			get = new PacketGetImpl("https://passport.ganji.com/login.php");
			get.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.154 Safari/537.36");
			get.addParams("username", username);
			get.addParams("second", "");
			get.addParams("next", getUrl());
			
			get.setResponse(wpp.execute(get));
			System.err.println(EntityUtils.toString(get.getResponse().getEntity()));
			get.getRequest().abort();
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	
	public static void main(String[] args) throws Exception {
		java.util.logging.Logger.getLogger("org.apache.http.headers").setLevel(
				java.util.logging.Level.FINER);

		System.setProperty("org.apache.commons.logging.Log",
				"org.apache.commons.logging.impl.SimpleLog");
		System.setProperty("org.apache.commons.logging.simplelog.showdatetime",
				"true");
		System.setProperty(
				"org.apache.commons.logging.simplelog.log.org.apache.http.headers",
				"debug");

		new Ganji().execute();
	}
	
}
