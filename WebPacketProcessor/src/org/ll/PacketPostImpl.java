package org.ll;

import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.http.HttpResponse;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.ll.iface.Packet;

public class PacketPostImpl implements Packet {

	
	private HttpPost request;
	private URIBuilder uriBuilder;
	private HttpResponse response;
	private ArrayList<BasicNameValuePair> pairs = new ArrayList<BasicNameValuePair>();
	

	public HttpPost getRequest() {
		return request;
	}

	public void setRequest(HttpPost request) {
		this.request = request;
	}

	public HttpResponse getResponse() {
		return response;
	}

	public void setResponse(HttpResponse response) {
		this.response = response;
	}

	@Override
	public boolean isReadyForSend() {
		return true;
	}

	@Override
	public void printResponseTo(OutputStream os) throws Exception {
		//os.println(EntityUtils.toString(response.getEntity()));
	}

	@Override
	public String getUrlString() {
		
		return null;
	}

	@Override
	public void setUrlString(String url) {
			request = new HttpPost(url);	
	}

	public PacketPostImpl(String uri) throws URISyntaxException {
		super();
//		this.uriBuilder = new URIBuilder(uri);
		setUrlString(uri);
	}
	
	@Override
	public void setContent(String content) {
		
	}
	
	public void addHeader(String name,String value) {
		request.addHeader(name, value);
	}
	
	@Override
	public void addParams(String name,String value) throws UnsupportedEncodingException {
		pairs.add(new BasicNameValuePair(name, value));
		request.setEntity(new UrlEncodedFormEntity(pairs));
	}

	@Override
	public void setHeaders(Properties properties) {
		for (Entry<Object, Object> entry : properties.entrySet()) {
			request.addHeader((String)entry.getKey(), (String)entry.getValue());
		}
	}

	@Override
	public void setReadyForSend(boolean boolean1) {
	}
	
	public void setUrlEncodedFormEntity(Properties properties) throws UnsupportedEncodingException {
		ArrayList<BasicNameValuePair> pairs = new ArrayList<BasicNameValuePair>();
		for (Entry<Object, Object> entry : properties.entrySet()) {
			pairs.add(new BasicNameValuePair((String)entry.getKey(), (String)entry.getValue()));
		}
		request.setEntity(new UrlEncodedFormEntity(pairs));
	}

}
