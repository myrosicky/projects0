package org.ll;

import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.ll.iface.Packet;




class PacketMaintainPanel extends JPanel {
	int g_jta_width = 140, g_jta_heigh = 40;
	JTextArea jta_url = new JTextArea("url"), jta_headers = new JTextArea(
			"headers"), jta_content = new JTextArea("content");
	JButton jb_save = new JButton("save"), jb_add = new JButton("add");
	boolean isHeaderChange = false;
	Properties parsedHeaders = new Properties();

	public PacketMaintainPanel() {
		super();

		setSize(g_jta_width * 3, g_jta_heigh * 6);
		setLayout(null);
		jta_content.setText("enter content");
		jta_headers.setText("enter headers");
		jta_url.setText("enter url");

		jta_url.setBounds(30, 10, g_jta_width, g_jta_heigh);
		jta_headers.setBounds(30, 60, g_jta_width, g_jta_heigh);
		jta_content.setBounds(30, 110, g_jta_width, g_jta_heigh);

		jta_headers.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent arg0) {
				isHeaderChange = true;
			}

			@Override
			public void insertUpdate(DocumentEvent arg0) {
				isHeaderChange = true;
			}

			@Override
			public void changedUpdate(DocumentEvent arg0) {
				isHeaderChange = true;
			}
		});

		jta_headers.addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent arg0) {
				if (isHeaderChange && jta_headers.getText().trim().length() > 0) {
					parsedHeaders = parseHeaders(jta_headers.getText());
				}
			}

			@Override
			public void focusGained(FocusEvent arg0) {
			}
		});

		add(jta_content);
		add(jta_headers);
		add(jta_url);

		jb_save.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				List<Packet> newPacketList = CIConvertor.getDefinedPackets();
				Packet muo;
				try {
					muo = new PacketPostImpl(jta_url.getText());
					muo.setContent(jta_content.getText());
					muo.setHeaders(parsedHeaders);
					muo.setReadyForSend(true);
					newPacketList.add(muo);
					CIConvertor.setDefinedPackets(newPacketList);
					JOptionPane.showMessageDialog(null, "save successfully");
				} catch (URISyntaxException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		
		
		
		jb_save.setBounds(30, 160, g_jta_width, g_jta_heigh);
		add(jb_save);
		
	}

	private Properties parseHeaders(String rawHeaders) {
		String[] headers = rawHeaders.split("\n");
		Properties props = new Properties();
		for (String header : headers) {
			String key = header.trim().split("\\s{1,}")[0];
			props.put(key, header.substring(header.indexOf(key) + key.length())
					.trim());
		}
		return props;
	}
}



public class UIPanel extends JFrame {

	
	JButton  jb_add = new JButton("add"), jb_send = new JButton("send");
	PacketMaintainPanel packetMaintainPanel;
	private WebPacketProcessor wpp;
	int g_jta_width = 140, g_jta_heigh = 40;

	public UIPanel() throws HeadlessException {
		super();
		setTitle("send packet");
		setAlwaysOnTop(true);
		setLayout(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(g_jta_width * 4, g_jta_heigh * 7);
		
		wpp = new WebPacketProcessor();
		/***********************
		 * panels construction
		 ***********************/
		packetMaintainPanel = new PacketMaintainPanel();
		
		jb_send.setBounds(g_jta_width, g_jta_heigh * 2, g_jta_width, g_jta_heigh);
		jb_send.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					
					wpp.sendAllPacketAsIE();
					//wpp.sendPacketAsIE();
					JOptionPane.showMessageDialog(null, "send success");
				} catch (Exception e1) {
					e1.printStackTrace();
					JOptionPane.showMessageDialog(null, e1.getMessage());
				}
			}
		});
		
		jb_add.setBounds(g_jta_width, g_jta_heigh * 3, g_jta_width, g_jta_heigh);
		jb_add.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				add(packetMaintainPanel);
				jb_add.setVisible(false);
			}
		});
		add(jb_send);
		add(jb_add);
		
	}

	public static void main(String[] args) throws Exception {
		new UIPanel().setVisible(true);
		//WebPacketProcessor wpp = new WebPacketProcessor();
		//wpp.sendPacketAsIE();
	}

}
