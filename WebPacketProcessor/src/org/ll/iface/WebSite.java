package org.ll.iface;

public abstract class WebSite {

	String url, name, contentFilePath;

	public String getContentFilePath() {
		return contentFilePath;
	}

	public void setContentFilePath(String contentFilePath) {
		this.contentFilePath = contentFilePath;
	}

	public String getName() {
		return name;
	}

	public WebSite(String name, String url, String contentFilePath) {
		super();
		this.url = url;
		this.name = name;
		this.contentFilePath = contentFilePath;
	}

	public void setName(String name) {
		if (name != null)
			this.name = name;
	}

	public String getUrl() {
		return url;
	}


	public void setUrl(String url) {
		if (url != null)
			this.url = url;
	}

	public abstract void execute() throws Exception;

}
