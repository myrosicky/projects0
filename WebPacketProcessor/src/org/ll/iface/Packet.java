package org.ll.iface;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.Properties;

import org.apache.http.client.methods.HttpUriRequest;

public interface Packet {
	public enum METHOD {
		POST,GET
	}
	
	public boolean isReadyForSend();
	public void printResponseTo(OutputStream os)  throws Exception ;
	public String getUrlString();
	public void setUrlString(String url) throws MalformedURLException, IOException;
	public void setContent(String content);
	public void setHeaders(Properties properties);
	public void setReadyForSend(boolean boolean1);
	void addParams(String name, String value) throws UnsupportedEncodingException;
	public HttpUriRequest getRequest();
}
