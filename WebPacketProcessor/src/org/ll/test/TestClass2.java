package org.ll.test;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

public class TestClass2 {

	public static void main(String[] args) {

		java.util.logging.Logger.getLogger("org.apache.http.headers").setLevel(
				java.util.logging.Level.FINER);

		System.setProperty("org.apache.commons.logging.Log",
				"org.apache.commons.logging.impl.SimpleLog");
		System.setProperty("org.apache.commons.logging.simplelog.showdatetime",
				"true");
		System.setProperty(
				"org.apache.commons.logging.simplelog.log.org.apache.http.headers",
				"debug");

		HttpClient httpClient = new DefaultHttpClient();
		ClientConnectionManager mgr = httpClient.getConnectionManager();
		HttpParams hp = httpClient.getParams();
		HttpGet get = null;
		HttpPost post = null;
		HttpResponse response = null;
		HttpParams httpParameters = null;
		try {

			post = new HttpPost("http://www.enigmagroup.org/");
			post.addHeader(
					"User-Agent",
					"Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; Trident/7.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)");
			response = httpClient.execute(post);
			String html = EntityUtils.toString(response.getEntity());
			// System.err.println(html);
			post.abort();

			/**************************************
			 * log on website
			 **************************************/
			post = new HttpPost("http://www.enigmagroup.org/forums/login2/");
			post.addHeader(
					"User-Agent",
					"Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; Trident/7.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)");
			// post.addHeader("Cookie",
			// "__utmc=115660889; __utma=115660889.570318590.1442556836.1442556836.1442556836.1; __utmz=115660889.1442556836.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); __utmb=115660889.4.10.1442556836; enigmafiedV4=a%3A4%3A%7Bi%3A0%3Bs%3A5%3A%2214832%22%3Bi%3A1%3Bs%3A40%3A%226a8349898782619579efc65cda606d7b4c9be939%22%3Bi%3A2%3Bi%3A1442560639%3Bi%3A3%3Bi%3A2%3B%7D; PHPSESSID=52vrt781ha6pcic1s0m3jlbuq6; __utmt=1");
			post.addHeader("Accept", "application/json, text/plain, */*");
			post.addHeader("Accept-Language", "zh-CN");
			post.addHeader(
					"User-Agent",
					"Mozilla/5.0 (iPad; CPU OS 4_3_2 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8H7 Safari/6533.18.5");
			post.addHeader("Content-Type", "application/x-www-form-urlencoded");
			post.addHeader("Accept-Encoding", "gzip, deflate");
			post.addHeader("Host", "www.enigmagroup.org");
			post.addHeader("Connection", "Keep-Alive");
			post.addHeader("Cache-Control", "max-age=0");
			post.addHeader("Origin", "http://www.enigmagroup.org");

			String user = "myfabregas", passwrd = "lL69767425", openid_identifier = "", hash_passwrd = "";
			ArrayList<BasicNameValuePair> pairs = new ArrayList<BasicNameValuePair>();
			pairs.add(new BasicNameValuePair("user", user));
			pairs.add(new BasicNameValuePair("passwrd", passwrd));
			pairs.add(new BasicNameValuePair("openid_identifier", ""));
			pairs.add(new BasicNameValuePair("cookielength", "-1"));

			post.setEntity(new UrlEncodedFormEntity(pairs));
			response = httpClient.execute(post);
			// System.err.println(EntityUtils.toString(response.getEntity()));
			post.abort();

			/**************************************
			 * enter mission page
			 **************************************/
			System.out.println("enter mission page");
			post = new HttpPost(
					"http://www.enigmagroup.org/missions/programming/3/image.php");

			response = httpClient.execute(post);
			post.abort();
			// html = EntityUtils.getContentCharSet(response.getEntity());
			// System.err.println(new String(EntityUtils.toByteArray(response
			// .getEntity())));
			BufferedImage image = ImageIO.read(response.getEntity()
					.getContent());
			System.out.println(image.getRGB(0, 0));
			int rgb = image.getRGB(0, 0), r = 0, g = 0, b = 0;
			r = (rgb >> 16) & 0xFF;
			g = (rgb >> 8) & 0xFF;
			b = (rgb) & 0xFF;
			System.out.println(r + ";" + g + ";" + b + ";");
			post = new HttpPost(
					"http://www.enigmagroup.org/missions/programming/3/image.php");

			httpParameters = new BasicHttpParams();
			httpParameters.setParameter("color", r + ";" + g + ";" + b);
			httpParameters.setParameter("submit", "1");
			post.setParams(httpParameters);
			response = httpClient.execute(post);
			System.out.println(EntityUtils.toString(response.getEntity()));

			post.abort();
			// System.out.println(image.getRGB(0, 0));
			// System.err.println(EntityUtils.toString(response.getEntity()));

			// post = new HttpPost("http://www.enigmagroup.org/forums/login2/");
			// ArrayList<BasicNameValuePair> pairs = new
			// ArrayList<BasicNameValuePair>();
			//
			// pairs.add(new BasicNameValuePair("user", "myfabregas"));
			// pairs.add(new BasicNameValuePair("passwrd", ""));
			// pairs.add(new BasicNameValuePair("openid_identifier", ""));
			// pairs.add(new BasicNameValuePair("cookielength", "60"));
			// pairs.add(new BasicNameValuePair("hash_passwrd",
			// "383c873ed56abe9f787db36b7b3d93052acc23ec"));
			// post.setEntity (new UrlEncodedFormEntity(pairs));
			// post.addHeader("Accept", "application/json, text/plain, */*");
			// post.addHeader("Accept-Language", "zh-CN");
			// post.addHeader("User-Agent",
			// "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; Trident/7.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)");
			// post.addHeader("Content-Type",
			// "application/x-www-form-urlencoded");
			// post.addHeader("Accept-Encoding", "gzip, deflate");
			// post.addHeader("Host", "www.enigmagroup.org");
			// post.addHeader("Connection", "Keep-Alive");
			// post.addHeader("Cache-Control", "no-cache");
			// response = httpClient.execute(post);
			// System.err.println(EntityUtils.toString( response.getEntity()));
			//
			// get = new
			// HttpGet("http://www.enigmagroup.org/missions/programming/1/");
			// httpParameters = new BasicHttpParams();
			// httpParameters.setParameter("ip", "219.137.26.182");
			// httpParameters.setParameter("username", "myfabregas");
			// get.setParams(httpParameters);
			// get.addHeader("User-Agent",
			// "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; Trident/7.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)");
			// get.addHeader("Cookie", "mission=yes");
			// response = httpClient.execute(get);
			//
			// String html = EntityUtils.toString( response.getEntity());
			// System.err.println(html);
			// get.abort();
			//
			// post = new
			// HttpPost("https://wx2.qq.com/cgi-bin/mmwebwx-bin/webwxsendmsg");
			// post.addHeader("Accept", "application/json, text/plain, */*");
			// post.addHeader("Accept-Language", "zh-CN");
			// post.addHeader("User-Agent",
			// "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; Trident/7.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)");
			// post.addHeader("Content-Type", "application/json;charset=utf-8");
			// post.addHeader("Accept-Encoding", "gzip, deflate");
			// post.addHeader("Host", "wx2.qq.com");
			// post.addHeader("Connection", "Keep-Alive");
			// post.addHeader("Cache-Control", "no-cache");
			// post.setEntity (new
			// StringEntity("{\"BaseRequest\":{\"Uin\":1688424243,\"Sid\":\"XlEzrcwzdrvrPRhl\",\"Skey\":\"@crypt_ec62f961_8483100720a659136b85b50c14890f80\",\"DeviceID\":\"e633084894927654\"},\"Msg\":{\"Type\":1,\"Content\":\"222\",\"FromUserName\":\"@3b534e86e2169e8cf08c4d6b08fc6e3f27f6b154f52ce5b1b599469febf8db7e\",\"ToUserName\":\"@d8a7a23ba78fffc289bcc4e7585958dfa31a4cbf69dce7b6e98c25615a89f35e\",\"LocalID\":\"14425540254860173\",\"ClientMsgId\":\"14425540254860173\"}}"));
			// response = httpClient.execute(post);
			// System.err.println(EntityUtils.toString( response.getEntity()));
			// post.abort();

		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			// get.abort();
			httpClient.getConnectionManager().shutdown();
		}

	}
}
