package org.ll.test;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.ll.PacketPostImpl;
import org.ll.WebPacketProcessor;

public class TestClass5 {
	
	public void test1() {


		java.util.logging.Logger.getLogger("org.apache.http.headers").setLevel(
				java.util.logging.Level.FINER);

		System.setProperty("org.apache.commons.logging.Log",
				"org.apache.commons.logging.impl.SimpleLog");
		System.setProperty("org.apache.commons.logging.simplelog.showdatetime",
				"true");
		System.setProperty(
				"org.apache.commons.logging.simplelog.log.org.apache.http.headers",
				"debug");

		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpPost post = null;
		HttpResponse response = null;
		try {

			post = new HttpPost("http://www.enigmagroup.org/");
			post.addHeader(
					"User-Agent",
					"Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; Trident/7.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)");
			response = httpClient.execute(post);
			String html = EntityUtils.toString(response.getEntity());
			// System.err.println(html);
			post.abort();

			/**************************************
			 * log on website
			 **************************************/
			post = new HttpPost("http://www.enigmagroup.org/forums/login2/");
			post.addHeader(
					"User-Agent",
					"Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; Trident/7.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)");
			// post.addHeader("Cookie",
			// "__utmc=115660889; __utma=115660889.570318590.1442556836.1442556836.1442556836.1; __utmz=115660889.1442556836.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); __utmb=115660889.4.10.1442556836; enigmafiedV4=a%3A4%3A%7Bi%3A0%3Bs%3A5%3A%2214832%22%3Bi%3A1%3Bs%3A40%3A%226a8349898782619579efc65cda606d7b4c9be939%22%3Bi%3A2%3Bi%3A1442560639%3Bi%3A3%3Bi%3A2%3B%7D; PHPSESSID=52vrt781ha6pcic1s0m3jlbuq6; __utmt=1");
			post.addHeader("Accept", "application/json, text/plain, */*");
			post.addHeader("Accept-Language", "zh-CN");
			post.addHeader(
					"User-Agent",
					"Mozilla/5.0 (iPad; CPU OS 4_3_2 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8H7 Safari/6533.18.5");
			post.addHeader("Content-Type", "application/x-www-form-urlencoded");
			post.addHeader("Accept-Encoding", "gzip, deflate");
			post.addHeader("Host", "www.enigmagroup.org");
			post.addHeader("Connection", "Keep-Alive");
			post.addHeader("Cache-Control", "max-age=0");
			post.addHeader("Origin", "http://www.enigmagroup.org");

			String user = "myfabregas", passwrd = "lL69767425", openid_identifier = "", hash_passwrd = "";
			ArrayList<BasicNameValuePair> pairs = new ArrayList<BasicNameValuePair>();
			pairs.add(new BasicNameValuePair("user", user));
			pairs.add(new BasicNameValuePair("passwrd", passwrd));
			pairs.add(new BasicNameValuePair("openid_identifier", ""));
			pairs.add(new BasicNameValuePair("cookielength", "-1"));

			post.setEntity(new UrlEncodedFormEntity(pairs));
			response = httpClient.execute(post);
			System.err.println(EntityUtils.toString(response.getEntity()));
			post.abort();

			/**************************************
			 * enter mission page
			 **************************************/
			System.out.println("enter mission page");
			post = new HttpPost(
					"http://www.enigmagroup.org/missions/programming/3/image.php");

			response = httpClient.execute(post);
			post.abort();
			BufferedImage image = ImageIO.read(response.getEntity()
					.getContent());
			System.out.println(image.getRGB(0, 0));
			int rgb = image.getRGB(0, 0), r = 0, g = 0, b = 0;
			r = (rgb >> 16) & 0xFF;
			g = (rgb >> 8) & 0xFF;
			b = (rgb) & 0xFF;
			System.out.println(r + ";" + g + ";" + b + ";");
			post = new HttpPost(
					"http://www.enigmagroup.org/missions/programming/3/image.php");

			pairs = new ArrayList<BasicNameValuePair>();
			pairs.add(new BasicNameValuePair("color", r + ";" + g + ";" + b));
			pairs.add(new BasicNameValuePair("submit", "1"));
			post.setEntity(new UrlEncodedFormEntity(pairs));
			response = httpClient.execute(post);
			System.err.println(EntityUtils.toString(response.getEntity()));

			post.abort();
			
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	
	public void test2() {


		java.util.logging.Logger.getLogger("org.apache.http.headers").setLevel(
				java.util.logging.Level.FINER);

		System.setProperty("org.apache.commons.logging.Log",
				"org.apache.commons.logging.impl.SimpleLog");
		System.setProperty("org.apache.commons.logging.simplelog.showdatetime",
				"true");
		System.setProperty(
				"org.apache.commons.logging.simplelog.log.org.apache.http.headers",
				"debug");

		WebPacketProcessor httpClient = new WebPacketProcessor();
		PacketPostImpl post = null;
		try {

			post =  new PacketPostImpl("http://www.enigmagroup.org/");
			post.addHeader(
					"User-Agent",
					"Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; Trident/7.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)");
			post.setResponse(httpClient.execute(post));
			post.getRequest().abort();

			/**************************************
			 * log on website
			 **************************************/
			post = new PacketPostImpl("http://www.enigmagroup.org/forums/login2/");
			post.addHeader(
					"User-Agent",
					"Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; Trident/7.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)");
			post.addHeader("Accept", "application/json, text/plain, */*");
			post.addHeader("Accept-Language", "zh-CN");
			post.addHeader(
					"User-Agent",
					"Mozilla/5.0 (iPad; CPU OS 4_3_2 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8H7 Safari/6533.18.5");
			post.addHeader("Content-Type", "application/x-www-form-urlencoded");
			post.addHeader("Accept-Encoding", "gzip, deflate");
			post.addHeader("Host", "www.enigmagroup.org");
			post.addHeader("Connection", "Keep-Alive");
			post.addHeader("Cache-Control", "max-age=0");
			post.addHeader("Origin", "http://www.enigmagroup.org");

			String user = "myfabregas", passwrd = "lL69767425", openid_identifier = "", hash_passwrd = "";
			post.addParams("user", user);
			post.addParams("passwrd", passwrd);
			post.addParams("openid_identifier", "");
			post.addParams("cookielength", "-1");
			post.setResponse(httpClient.execute(post));
			//post.printResponseTo(System.err);
			post.getRequest().abort();

			/**************************************
			 * enter mission page
			 **************************************/
			System.out.println("enter mission page");
			post  = new PacketPostImpl(
					"http://www.enigmagroup.org/missions/programming/3/image.php");

			post.setResponse(httpClient.execute(post));
			post.getRequest().abort();
			BufferedImage image = ImageIO.read(post.getResponse().getEntity()
					.getContent());
			System.out.println(image.getRGB(0, 0));
			int rgb = image.getRGB(0, 0), r = 0, g = 0, b = 0;
			r = (rgb >> 16) & 0xFF;
			g = (rgb >> 8) & 0xFF;
			b = (rgb) & 0xFF;
			System.out.println(r + ";" + g + ";" + b + ";");
			post = new PacketPostImpl(
					"http://www.enigmagroup.org/missions/programming/3/image.php");
			
			post.addParams("color", r + ";" + g + ";" + b);
			post.addParams("submit", "1");
			post.setResponse(httpClient.execute(post));
			System.err.println(EntityUtils.toString(post.getResponse().getEntity()));
			post.getRequest().abort();
			
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	
	public void test3() {
		
	}
	
	
	public static void main(String[] args) {
		new TestClass5().test3();
		
	}
}
