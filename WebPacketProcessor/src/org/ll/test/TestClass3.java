package org.ll.test;

import java.lang.ref.WeakReference;


public class TestClass3 {

	class a{
		String nameString;

		public a(String nameString) {
			super();
			this.nameString = nameString;
		}
		
	}
	class b{
		a aobj;

		public b(a aobj) {
			super();
			this.aobj = aobj;
		}
		
	}
	
	class c{
		a aobj;
		public c(a aobj) {
			super();
			this.aobj = aobj;
		}
	}
	
	
	public static void main(String[] args) {
		TestClass3 t3 = new TestClass3();
		a aobj = t3.new a("aa");
		WeakReference<a> weakaobj = new WeakReference<TestClass3.a>(aobj);
		b bobj = t3.new b(aobj);
		c cobj = t3.new c(aobj);
		aobj = null;
		System.err.println(cobj.aobj);
	}
}
